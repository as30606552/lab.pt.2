import socket
from server import PORT, send, receive
from game import MAX_ANGLE

BUFFER_SIZE = 2 ** 10


def exit_client(code, s):
    s.close()
    print('Stopping the client')
    exit(code)


if __name__ == "__main__":
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    while True:
        try:
            client_socket.connect((socket.gethostname(), PORT))
            break
        except (socket.error, OverflowError):
            pass
    message = {}
    try:
        message = receive(client_socket)
    except (ConnectionAbortedError, ConnectionResetError):
        exit_client(1, client_socket)
    username = message['username']
    print(f'You entered the game under the name {username}')
    if message['action'] == 'continue':
        print('Game continued from the last save')
    elif message['action'] == 'begin':
        print('A new game is started')
    else:
        exit_client(1, client_socket)
    print(f'Distance to the target: {message["distance"]}')
    while True:
        try:
            message = receive(client_socket)
        except (ConnectionAbortedError, ConnectionResetError):
            exit_client(1, client_socket)
        if message['action'] == 'move':
            angle = input('Your turn. Enter the angle: ')
            while type(angle) != int:
                try:
                    angle = int(angle)
                    if angle < 0 or angle > MAX_ANGLE:
                        raise ValueError()
                except ValueError:
                    angle = input('Invalid input. Try again: ')
            send(client_socket, {
                'angle': angle
            })
            try:
                message = receive(client_socket)
            except (ConnectionAbortedError, ConnectionResetError):
                exit_client(1, client_socket)
            print(f'Your new distance: {message["distance"]}. Wait for your turn')
        elif message['action'] == 'end':
            if message['type'] == 'win':
                if message['winner'] == username:
                    print('The game is over! You won!')
                else:
                    print('The game is over! You lost, but you will definitely succeed next time!')
            elif message['type'] == 'draw':
                print('The game is over! Draw!')
            exit_client(0, client_socket)
