import math
from random import random
import json
import jsonschema
from jsonschema import validate

NUMBER_OF_PLAYERS = 3
MAX_INITIAL_DISTANCE = 4
MIN_INITIAL_DISTANCE = 2
MAX_ANGLE = 360

schema = {
    'type': 'object',
    'properties': {
        'players': {
            'type': 'array',
            'minItems': NUMBER_OF_PLAYERS,
            'maxItems': NUMBER_OF_PLAYERS,
            'items': [
                {
                    'type': 'object',
                    'properties': {
                        'username': {'type': 'string'},
                        'x': {'type': 'number'},
                        'y': {'type': 'number'}
                    },
                    'required': [
                        'username',
                        'x',
                        'y'
                    ],
                    'additionalProperties': False
                }
            ]
        }
    },
    'required': [
        'players'
    ]
}


def calculate_distance():
    return MIN_INITIAL_DISTANCE + (MAX_INITIAL_DISTANCE - MIN_INITIAL_DISTANCE) * random()


def create_player_from_polar_coordinates(username, distance, angle, client):
    angle = math.radians(angle)
    x = distance * math.cos(angle)
    y = distance * math.sin(angle)
    return Player(username, x, y, client)


def parse_data(data):
    players = []
    for player_data in data['players']:
        player = Player(player_data['username'], player_data['x'], player_data['y'], None)
        players.append(player)
    return players


def init_players():
    distance = calculate_distance()
    players = []
    for i in range(NUMBER_OF_PLAYERS):
        players.append(create_player_from_polar_coordinates(f'Player {i}', distance, MAX_ANGLE * random(), None))
    return players


def validate_json(data):
    try:
        validate(instance=data, schema=schema)
    except jsonschema.exceptions.ValidationError:
        return False
    return True


def load_game_from_json(json_file_path):
    with open(json_file_path) as json_file:
        try:
            data = json.load(json_file)
            if validate_json(data):
                return data
        except json.decoder.JSONDecodeError:
            pass
    return None


def dump_game_to_json(players, json_file_path):
    data = {'players': []}
    for player in players:
        data['players'].append(player.to_dict())
    with open(json_file_path, 'w') as output:
        json.dump(data, output, indent=4)


class Player:
    def __init__(self, username, x, y, client_socket):
        self.username = username
        self.x = x
        self.y = y
        self.client_socket = client_socket

    def __str__(self):
        return f'Player username: {self.username}, x: {self.x}, y: {self.y}'

    def get_distance(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def move(self, angle):
        angle = math.radians(angle)
        x = math.cos(angle)
        y = math.sin(angle)
        self.x += x
        self.y += y

    def is_won(self):
        return self.get_distance() <= 1

    def to_dict(self):
        return {
            'username': self.username,
            'x': self.x,
            'y': self.y
        }
